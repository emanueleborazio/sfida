import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { CronoPageComponent } from './crono-page/crono-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomePageComponent } from './home-page/home-page.component';
import { CronoServiceS } from './service/crono-s.service';
import { HttpClientModule } from '@angular/common/http';
import { TestComponent } from './test/test.component';


export const appRoutes: Routes = [
  // { path: '',redirectTo: '/homePage', pathMatch: 'full', component: HomePageComponent },
  { path: 'cronoPage', component: CronoPageComponent },
  { path: 'testPage', component: TestComponent },
  { path:'', redirectTo: 'homePage', pathMatch: 'full', } ,
  { path: 'homePage', component: HomePageComponent}
  
]; 
 

@NgModule({
  declarations: [
    AppComponent,
    CronoPageComponent,
    HomePageComponent,
    TestComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
 
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [
    CronoServiceS
  ],
  bootstrap: [AppComponent],
 
})
export class AppModule { }
