import { TestBed } from '@angular/core/testing';

import { CronoServiceS } from './crono-s.service';

describe('CronoServiceS', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CronoServiceS = TestBed.get(CronoServiceS);
    expect(service).toBeTruthy();
  });
});
