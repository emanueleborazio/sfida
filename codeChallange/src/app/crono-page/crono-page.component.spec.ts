import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CronoPageComponent } from './crono-page.component';

describe('CronoPageComponent', () => {
  let component: CronoPageComponent;
  let fixture: ComponentFixture<CronoPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CronoPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CronoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
