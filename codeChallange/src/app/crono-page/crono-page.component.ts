import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OnDestroy } from '@angular/core';
import { CronoServiceS } from '../service/crono-s.service';

@Component({
  selector: 'app-crono-page',
  templateUrl: './crono-page.component.html',
  styleUrls: ['./crono-page.component.css']
})
export class CronoPageComponent implements OnInit {
  codiceUtente: string = "";
 

  

  public ore:number = 0;
  public minuti:number = 0;
  public secondi:number = 0;

  public con=0;
  public colection:Array<any> = [];
  public contatore:any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private cronoService: CronoServiceS
  ) { }

  ngOnInit() {
   
    this.codiceUtente = this.route.snapshot.queryParamMap.get('codice');
    

  }
  start(){
    if(this.contatore == undefined) {
        this.contatore = setInterval(()=> {
                this.secondi += 1;
                if (this.secondi == 60) {
                    this.secondi = 0;
                    this.minuti += 1;
                    if (this.minuti == 60) {
                        this.minuti = 0;
                        this.ore += 1;
                        if (this.ore = 24) {
                            this.ore = 0;
                        }
                    }
                }
            }
            , 100);
    }
}



reset(){
  clearInterval(this.contatore);
  this.ore = 0;
  this.minuti = 0;
  this.secondi = 0;
  this.contatore = null;
}


check(){
  this.router.navigate(['/testPage']);
}



consegna(){
    let obj:any = {};
    obj.ore = this.ore;
    obj.minuti = this.minuti;
    obj.secondi = this.secondi;

    console.log("minuti: "+ obj.minuti + "secondi: " + obj.secondi)

    clearInterval(this.contatore);

    this.cronoService.getInviaDati().subscribe(
      res => {
        console.log(res)
      },
      err => {
        console.log(err)
      }
      
      

    )

    
}

// lapso(){
//     let obj:any = {};
//     obj.ore = this.ore;
//     obj.minuti = this.minuti;
//     obj.secondi = this.secondi;

//     this.con += 1;
//     this.colection.push(obj);
// }
// stop(){
//     clearInterval(this.contatore);
//     this.ore = 0;
//     this.minuti = 0;
//     this.secondi = 0;
//     this.contatore = null;
// }
  

}
