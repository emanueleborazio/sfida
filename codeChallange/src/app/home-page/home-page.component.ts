import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  title = 'codeChallange';
  fgInserimentoCodice: FormGroup = new FormGroup({});
  codiceUtente : string = ""

  constructor(
    private router: Router

  ) { }

  ngOnInit() {
   
    
    this.fgInserimentoCodice.addControl('codUtente', new FormControl(null, [Validators.required]));

    

  }



  CronoPage() {

    console.log("click")
    this.codiceUtente = this.fgInserimentoCodice.value.codUtente;
    console.log(this.codiceUtente)
    this.router.navigate(['/cronoPage'],
    {
        queryParams: {
           codice: this.codiceUtente

        }
    });
  }
}