export class Par {
    grant_type: string
    client_id: string
    client_secret: string
    password: string
    username: string
    constructor(grant_type,client_id,client_secret,password,username){
        this.grant_type = grant_type
        this.client_id = client_id
        this.client_secret = client_secret
        this.password = password
        this.username = username
    
    }
}

